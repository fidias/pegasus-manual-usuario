# Área do Cliente

A **Área do Cliente** fica disponível na primeira tela após fazer login. Lá, é
possível desbloquear o sistema temporariamente e baixar 2º via de boletos em
aberto.

![area cliente](./img/area_cliente.png)

## Desbloquear temporariamente

Para desbloquear o sistema temporariamente:

1. Clicar no botão **Informar Pagamento**.
    :::tip

    - As linhas em vermelho representam os boletos vencidos.

    - As linhas em preto representam os boletos pagos.

    - As linhas em amarelo representam os boletos próximo do vencimento.

    :::

    ![informar pagamento](./img/informar_pagamento.png)

2. Clicar em confirmar.

    ![confirmar pagamento](./img/confirmar_pagamento.png)

Após isso, é necessário sair do sistema e entrar novamente.

:::tip NOTA:
O sistema será desbloqueado por **10 dias** até que seja feito o pagamento do
boleto em aberto. Caso o pagamento não seja efetuado, o sistema bloqueará
novamente.
:::

## Baixar 2º via de boleto

1. Clicar no botão **Baixar 2º via**.

    ![2via](./img/2via.png)
