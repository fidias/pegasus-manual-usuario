# Gerar Arquivo XML para a contabilidade

## Gerar arquivo xml

1. Crie uma pasta na área de trabalho com o nome **XML**.
![Criar pasta XML](./img/criar_pasta_xml.gif)

2. Abra o programa **Exportar DFe** que está na Área de Trabalho.
![Abrir Exportar DFe](./img/abrir_exportar_dfe.gif)

3. Clique no botão **Navegar**, vá até a pasta criada no passo 1 e selecione-a.
![Navegar pasta XML](./img/nav_pasta_xml.gif)

4. Clique no botão **Salvar XML** para gerar o arquivo `.zip` com os XMLs para
   enviar para a contabilidade.
![Salvar XML](./img/salvar_xml.gif)

:::tip NOTAS:

- Um arquivo `.zip` deverá ser criado na pasta indicada no **passo 3**. O nome
  do arquivo é a data inicial e a data final tudo junto no formato `dia, mês e
  ano`. Exemplo: `DFe-01052022-31052022` onde `01 05 2022` e `31 05 2022`.

- É recomendável renomear o arquivo `.zip` e a pasta dentro do arquivo `.zip`
  trocando a palavra `DFe` pela razão social do estabelecimento. Ex: `Fidias Software-01052022-31052022`.

- Após geração dos arquivos é necessário enviá-los para a contabilidade via
  e-mail (no final deste passo a passo tem um vídeo executando os passos acima
  até o envio dos arquivos via e-mail usando o Gmail).

:::

## Vídeo executando o processo completo

<iframe width="760" height="430" src="https://www.youtube.com/embed/PrCTduL4kyI"
title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
</iframe>
