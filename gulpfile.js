const gulp = require('gulp')
const zip = require('gulp-zip')

// Optimize ui build (fractal)
const options = {
    cwd: './.vuepress/dist'
}

gulp.task('compress', () =>
    gulp.src('**/*', options)
        .pipe(zip('pegasus-manual-usuario.zip'))
        .pipe(gulp.dest('.'))
)
