---
home: true
heroImage: /img/logo-vector.svg
---

<div class="features">
  <div class="feature">
    <h2>Pegasus Web</h2>
    <p>Gerência da sua loja</p>
    <a href="/web/" class="btn-primary">Acessar</a>
  </div>
  <div class="feature">
    <h2>Pegasus Frente de Caixa</h2>
    <p>Vendas, Pedidos, Orçamentos</p>
    <a href="/frente-caixa/" class="btn-primary">Acessar</a>
  </div>
  <div class="feature">
    <h2>Outros</h2>
    <p>Gerar arquivos XML e etc.</p>
    <a href="/outros/" class="btn-primary">Acessar</a>
  </div>
  <div class="feature">
    <h2>Sistema Operacional</h2>
    <p>Instalação de drivers, programas de terceiros, etc.</p>
    <a href="/so/" class="btn-primary">Acessar</a>
  </div>
</div>
