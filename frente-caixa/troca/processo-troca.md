# Processo de Troca

O processo de troca é a **junção de duas ações**. São elas:

1. **Devolução de Mercadoria** para o estoque.
1. **Venda de Mercadoria** utilizando o crédito da troca.

## Devolução de Mercadoria

### Iniciar Devolução de Mercadoria

Para iniciar uma **Troca** é necessário que o caixa esteja **livre**. Em
seguida, clicar no menu `Venda Balcão` e selecionar o submenu `Troca`.

![Ir Para Troca](./img/iniciar-troca/1-ir-para-troca.png)

Quando a tela de troca for aberta, devem ser preenchidos os campos: **Nome do
cliente (ou CPF ou CNPJ)**, **Motivo da troca**, o número da venda no
campo **Venda Balcão** e informar em qual caixa foi feito a compra no
campo **Caixa** e apertar a tecla <kbd>Enter</kbd>.

Se existir uma **Venda** com o número informado no **Caixa** informado, devem
aparecer os produtos e suas quantidades disponíveis para realizar uma
**Devolução**.

![Selecionar Cupom](./img/iniciar-troca/2-selecionar-cupom.png)

Use as teclas de navegação <kbd>&uarr;</kbd> ou <kbd>&darr;</kbd> ou digite o
_Código de Barras_ ou a _Referência do produto_ para selecionar o
produto que será **Devolvido**. Se caso a quantidade do produto a ser
devolvido seja **diferente da quantidade que foi vendida**, então **apague**
(usando a tecla <kbd>backspace</kbd>) a quantidade indicada na caixa que indica
a _**quantidade** a ser devolvida_  e digite a
_**quantidade desejada**_.

![Selecionar Produtos](./img/iniciar-troca/3-selecionar-produto-apagar-quantidade.png)

![Informar Quantidade](./img/iniciar-troca/4-informar-quantidade-a-devolver.png)

Em seguida aperta a tecla de navegação <kbd>→</kbd> para mover o produto para a
lista **ITENS DA TROCA** para ser devolvido ou <kbd>←</kbd> para remover o produto
da lista **ITENS DA TROCA**.

![Mover Para Devolucao](./img/iniciar-troca/5-movendo-para-devolucao.png)

Se já tiver concluído, ou seja, não tiver mais produtos que serão devolvidos,
então aperte a tecla <kbd>Enter</kbd>. Deverá aparecer uma tela de confirmação
perguntando se deseja realmente concluir a **Devolução de Mercadoria**.

![Confirmar Devolucao](./img/iniciar-troca/6-confirmando-devolucao.png)

Caso a resposta seja _Sim_ aparecerá uma outra perguntando se quer gerar um
comprovante de **Devolução**.

![Gerar Comprovante](./img/iniciar-troca/7-gerar-comprovante.png)

E ao final será gerado um crédito para o cliente no valor total de itens
devolvidos.

### Concluir Devolução de Mercadoria para o Estoque

Para concluir a **Devolução de Mercadoria para o Estoque** abra o sistema
Pegasus no navegador e vá até **Devolução de Mercadoria**. Para isso:

1. Clique no menu **Compra** (o que tem a cestinha de compras);
1. Clique no menu **Arquivo** e depois **Devolução de mercadoria**.

![Menu Compras](./img/finalizar-troca/1-menu-item-compras.png)
![Ir para Devolucao](./img/finalizar-troca/2-ir-para-devolucao-mercadoria.png)


Uma página listando todas as **Devoluções** a serem concluídas será aberta. Pelo
número do cupom que originou a devolução, clique no ícone de verificação (ícone
amarelo) na coluna _Ações_ da devolução que deseja concluir.

![Listar Devoluções](./img/finalizar-troca/3-listar-devolucoes.png)

Após clicar no botão para concluir a devolução, será aberta uma tela de compra
para dar entrada no estoque. A natureza da operação deve ser **Devolução de
venda de mercadoria adquirida ou recebida de terceiros**. Verifique se os
produtos e suas quantidades estão corretos e clique no botão **Concluir**.

![Seção Natureza de Operação](./img/finalizar-troca/4-natureza-operacao.png)
![Seção Produtos](./img/finalizar-troca/5-listar-produtos.png)

Deverá aparecer uma caixa perguntando se realmente deseja concluir a entrada.
Confirme clicando na opção **Ok**.

![Confirmar Concluir](./img/finalizar-troca/6-confirmar-concluir.png)

Se tiver dado tudo certo, você será redirecionado para lista de entradas de
produtos no estoque e a entrada que acabou de concluir deverá aparecer na
primeira linhas com as letras na cor _preto_.

![Listar Entradas](./img/finalizar-troca/7-listar-entrada-concluida.png)

## Realizar Venda usando Crédito do Cliente (Devolução)

Faça uma venda normal e aperte a tecla <kbd>F7</kbd> e em seguida as teclas
<kbd>ALT + T</kbd>. Deverá aparecer a tela como na imagem abaixo. Selecione o
cliente e use o crédito de acordo com o valor da nova compra.

![Usar Crédito](./img/usar-credito/1-usar-credito.png)

Finalize a venda normalmente.
