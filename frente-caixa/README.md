# Pegasus Frente de Caixa

Aplicativo _desktop_ para realização de **Vendas**, Orçamentos, Pedidos e Ordens de Serviços.

É operado por um caixa e fica na frente da sua Loja.

![Frente de Caixa](./img/frente-caixa.png)

## Login

Para entrar no sistema é necessário ser um [Usuário](/web/usuario/README.md).
Um usuário é criado a partir de um [Funcionário](/web/funcionario/README.md).

## Vendas

A tela de Vendas é uma das principais telas, responsável pelo controle de saída de
estoque entrada do caixa, através de formas de pagamento.

[Ir para Vendas](vendas/README.md)

## Menu F10

O menu **F10** oferece opções para o gerais para o Caixa.

[Ir para Menu F10](menu-principal/F10.md)

## Menu IR PARA

O menu **IR PARA** permite navegar entre outros módulos. São eles:

- [OS / Pedido / Orçamento](./dav/README.md)
- [Frente de Caixa](vendas/README.md)

## Menu MF-e

O menu **MF-e** oferece opções de gerência e controle do equipamento.

[Ir para Menu MF-e](menu-principal/mf-e.md)

## Processo de Troca

[Ir para Processo de Troca](troca/processo-troca.md)
