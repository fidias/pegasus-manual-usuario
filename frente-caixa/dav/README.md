# DAV (Documento Auxiliar de Venda)

![DAV - Lista](./img/dav-lista.png)

## Menu do Topo

![DAV - Lista Top Menu](./img/dav-lista-top-menu.png)

## Atalhos

* <kbd>F2</kbd> - [Novo](./novo-dav.md)
* <kbd>F7</kbd> - Fechamento DAV - Aguardando Faturamento
* <kbd>Alt + X</kbd> - Exportar (PDF) item selecionado
* <kbd>Alt + I</kbd> - Imprimir item selecionado
* <kbd>Alt + Q</kbd> - Arquivar item selecionado
* <kbd>Alt + T</kbd> - Listar todos os Documentos, seja PE, ORC ou DAV-OS
* <kbd>Alt + S</kbd> - Listar somente DAV-OS (Ordem de Serviço)
* <kbd>Alt + P</kbd> - Listar somente PE (Pedidos)
* <kbd>Alt + O</kbd> - Listar somente ORC (Orçamentos)
* <kbd>Alt + L</kbd> - Buscar por cliente
* <kbd>Alt + N</kbd> - Buscar por código do DAV
* <kbd>Alt + B</kbd> - Buscar por Objeto (Placa, Descrição)
* <kbd>ESC</kbd> - Voltar a tela de [Venda](../vendas/README.md)
