# DAV - Novo

![DAV - Novo](./img/dav-novo.png)

## Atalhos

* <kbd>F7</kbd> - [Fechamento DAV - Aguardando Faturamento](./fechamento-dav.md)
* <kbd>Alt + R</kbd> - Alterna busca por Referência ou Padrão
* <kbd>Alt + E</kbd> - Remove o item descrito com a coluna **NÚM** na busca
* <kbd>Alt + P</kbd> - Imprimir comanda
* <kbd>Alt + Q</kbd> - Arquivar
* <kbd>Alt + S</kbd> - Salvar Alterações
* <kbd>Alt + I</kbd> - Imprimir
* <kbd>Alt + X</kbd> - Exportar (PDF)
* <kbd>Alt + D</kbd> - [Variação percentual (Acréscismo/Desconto)](./variacao-percentual-item.md)
* <kbd>ESC</kbd> - Voltar a tela de [DAV](./README.md)
