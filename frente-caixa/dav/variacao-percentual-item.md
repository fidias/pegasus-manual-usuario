# Variação Percentual (Desconto/Acréscimo)

Defina o item, em seguida defina o valor da variação.

![Variação Percentual](./img/dav-variacao-percentual-item.png)

## Atalhos

* <kbd>Alt + C</kbd> - Cancelar variação de Desconto/Acréscimo
