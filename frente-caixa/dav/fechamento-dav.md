# Fechamento DAV - Aguradando Faturamento

Ao finalizar um DAV para aguardar faturamento, o operador pode dar
desconto/acréscimo diretamento no valor total, ou somente nos produtos
ou serviços.

## Faturar Ordem de Serviço

![Faturar OS](./img/dav-fechamento-ordem-servico-F7.png)

## Faturar Pedido

![Faturar PE](./img/dav-fechamento-pedido-F7.png)

## Atalhos

* <kbd>Alt + C</kbd> - Cancelar Desconto/Acréscimo

Navegue entre os campos usando <kbd>Enter</kbd>. Ou ainda <kbd>&darr;</kbd>
ou <kbd>&uarr;</kbd>.
