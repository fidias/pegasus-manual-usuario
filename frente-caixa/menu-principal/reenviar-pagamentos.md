# Reenviar Pagamentos

Tela responsável pode reenviar pagamentos pendentes no cartão, quando computador
tiver sem _Internet_ no momento da venda.

Essa tela realiza o mesmo processo de venda no cartão para os pagamentos que
ficaram pendentes e o reenvio deve ser executado logo que volte a _Internet_.

![Reenviar Pagamentos](./img/lista-pagamentos-pendentes.png)

## Atalhos

* <kbd>F7</kbd> - Reenviar todos os pagamentos pendentes
* <kbd>ESC</kbd> - Fechar janela
* Duplo clique - Reenviar pagamento selecionado
