---
prev: ./F10.md
---

# Vendas Efetuadas

Lista de Vendas (ou Trocas) efetuadas em um determinado dia, por um determinado
caixa.

![Vendas Efetuadas](./img/vendas-efetuadas.png)

## Opções

* ![exportar](./img/exportar.png) - Exporta Recibo em formato PDF
* ![imprimir](./img/imprimir.png) - Solicita reimpressão de cupom
* <kbd>Alt + C</kbd> - Cancela Venda selecionada
* <kbd>ESC</kbd> - Fechar janela

## Cancelamento de CF-e

Uma venda relacionada a um Cumpo Fiscal Eletrônico (CF-e), realizada através do
[MF-e](/frente-caixa/vendas/mf-e.md), só poderá ser cancelada no prazo
máximo de 30 minutos.
