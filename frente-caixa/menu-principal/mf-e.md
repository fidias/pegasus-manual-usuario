# Menu MF-e

O menu MF-e oferece opções de gerência e controle do equipamento.

![Menu MF-e](./img/menu-mfe.png)

## Opções

* [Reenviar Pagamentos](./reenviar-pagamentos.md)
* Consultar Status Operacional
* Visualizar Logs
