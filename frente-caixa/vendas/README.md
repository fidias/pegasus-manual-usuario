# Vendas

A tela de Vendas é uma das principais telas, responsável pelo controle de saída de
estoque entrada do caixa, através de formas de pagamento.

![Frente de Caixa](../img/frente-caixa.png)

## Atalhos

* <kbd>F2</kbd> - Nova Venda
* <kbd>F5</kbd> - Consulta de produtos
* <kbd>F9</kbd> - Ligar/Desligar impressora
* <kbd>F11</kbd> - Alterna busca por produto somente no início e no meio
* <kbd>F12</kbd> - Importar [DAV](/frente-caixa/dav/README.md)
* <kbd>Alt + C</kbd> - Cancelar cupom (aberto ou o último)
* <kbd>Alt + D</kbd> - Desconto/Acréscimo no Item (Venda Aberta)
* <kbd>Alt + G</kbd> - Abrir gaveta manualmente
* <kbd>ESC</kbd> - Solicitar saída do sistema

### Após venda aberta

* <kbd>F7</kbd> - Finalizar Venda à Vista
* <kbd>F8</kbd> - Finalizar Venda à Prazo
* <kbd>Alt + R</kbd> - Habilitar/Desabilitar busca de produto por refêrencia

## Venda à Vista

Fornece meios para informar forma de pagamento à vista, em dinheiro, cheque ou
cartão.

[Ir para Pagamento à vista](pagamento-a-vista.md)
