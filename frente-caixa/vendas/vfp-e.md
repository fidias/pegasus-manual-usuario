# VFP-e

Sigla para Validador Fiscal Eletrônico, é um projeto de apoio ao contribuinte,
com gestão de segurança anti-sonegação.

## Objetivos

* Registra vendas e certifica pagamentos
* Estatística e controle de recebíveis
* Auditoria e rastreabilidade de pagamento
* Análise e controle antifraude conciliador de pagamentos

## Referências

* <http://www.crc-ce.org.br/crcnovo/files/Apresentacao_MFE.pdf>
