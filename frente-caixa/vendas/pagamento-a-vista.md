# Pagamento à vista

![Pagamento à vista](./img/venda-pagamento-a-vista.png)

## Atalhos

* <kbd>ENTER</kbd> ou <kbd>&downarrow;</kbd> - Navegar para próximo campo
* <kbd>Shift + TAB</kbd> ou <kbd>&uparrow;</kbd> - Navegar para campo anterior
* <kbd>Alt + C</kbd> - Detalhes do Pagamento no Cartão
* <kbd>Alt + T</kbd> - Pagamento em Crédito do Cliente
* <kbd>ESC</kbd> - Fechar janela, volta para lista de itens
* <kbd>ESC</kbd> No campo do cliente - Deleta cliente definido para venda

## Pagamento em Dinheiro

Informe o total recebido do cliente e o sistema irá calcular quanto será o troco.

## Opções de Desconto

No campo **Total** é possível dar desconto ou acréscimo ao:

* Colocar valor diretamente:
    * Exemplo: O total é **R$ 100,00** e você quer deixar por
    **R$ 90,00**, basta digitar **90**.
* Colocar valor em porcentagem:
    * Exemplo: O total é **R$ 100,00** e você quer dar um
    desconto de **10%**, basta digitar **-10**.
    * O mesmo vale para acréscimo, **+10** dá um acréscimo
    de **10%**.

## Pagamento em Cartão

![Definir Cartão](./img/forma-pagamento-definir-cartao.png)

Cada Bandeira pode ser definida usando o teclado clicando no número correspondente.
Por exemplo para selecionar MasterCard basta clicar em <kbd>3</kbd>.

::: tip Dica sobre os atalhos
Devido a lista de Cartão ser passível de Habilitar/Desabilitar, os atalhos
são criados dinamicamente. Ou seja, nem sempre o atalho para _MasterCard_
será <kbd>3</kbd>.
:::

Após selecionar a Bandeira deve ser informado o valor pago naquele cartão.

![Informar Valor no Cartão](./img/forma-pagamento-cartao-valor.png)

Caso o valor informado for menor que o total, a tela anterior será reaberta para
permitir informar o valor restante, até que o valor total seja a soma de todos
os valores no cartão.

## Pagamento de cliente da loja

Em **Dados do Cliente** é possível informar um [Cliente](/web/cliente/README.md)
através do CPF/CNPJ ou o nome.

## Finalizar Pagamento

Ao clicar no botão Finalizar, o sistema irá fazer comunicação com o Integrador
MF-e. O Caixa irá acompanhar o processo através de um diálogo de progresso.

### Problemas de comunicação com Pagamento no Cartão

O pagamento no cartão pode ocorrer normalmente com o POS, mas a comunicação do
[MF-e](./mf-e.md) com as credenciadoras do cartão ([VFP-e](./vfp-e.md)) pode falhar.
Caso isso aconteça o sistema irá permitir ao Caixa informar os dados do pagamento em cartão
manualmente.

O Caixa será solicitado a **Tentar Novamente** ou **Informar Manualmente**.
Ao optar por **Informar Manualmente**, a tela abaixo irá aparecer para entrada
dos dados do pagamento.

![Dados Pagamento Cartão Manual](./img/dados-pagamento-cartao-manual.png)

#### Atalhos

* <kbd>ENTER</kbd> ou <kbd>&downarrow;</kbd> - Navegar para próximo campo
* <kbd>Shift + TAB</kbd> ou <kbd>&uparrow;</kbd> - Navegar para campo anterior
* <kbd>ESC</kbd> - Fechar janela, cancela pagamento manual e tenta realizar o processo normalmente

#### Identificação dos dados no comprovante

Segue imagens dos comprovantes de várias máquina para ajudar na identificação
dos campos exigidos pelo [MF-e](./mf-e.md):

![Comprovante Rede](./img/comprovante-rede.jpeg)

![Comprovante Getnet](./img/comprovante-getnet.jpeg)

![Comprovante Cielo](./img/comprovante-cielo.jpeg)

![Comprovante Elavon](./img/comprovante-elavon.jpeg)
