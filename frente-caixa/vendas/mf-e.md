# MF-e

Sigla para Módulo Fiscal Eletrônico, equipamento responsável por realizar
comunicação entre o contribuinte e a Sefaz, para o envio de Cupom Fiscal
Eletrônico (CF-e).

A comunicação é feita através do Integrador Fiscal, que é uma plataforma
de comunicação disponibilizada pelo Estado do Ceará para a integração de
AC/PDV's dos estabelecimentos contribuintes do ICMS do estado.

## Modelo Operacional

![Modelo Operacional](./img/modelo-operacional-integrador.png)

## Referências

* <http://cfe.sefaz.ce.gov.br/mfe>
* <http://www.crc-ce.org.br/crcnovo/files/Apresentacao_MFE.pdf>
