#!/usr/bin/env bash

set -o nounset
set -o errexit
set -o pipefail

npm run build

echo 'Enviando ao EC2 ...'
scp -i "$SSH_PEM_FILE" ./pegasus-manual-usuario.zip \
    ubuntu@fidias.com.br:"$SERVER_WWW_DIR"

echo 'Extraindo no EC2 ...'
ssh -i "$SSH_PEM_FILE" ubuntu@fidias.com.br "$SERVER_DEPLOY_SCRIPT"
echo 'Feito.'
