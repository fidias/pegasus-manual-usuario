---
prev: /web/
---

# Contas a Pagar

<span class="img-96">
    <img src="/img/modulo/contas-pagar.svg" alt="contas pagar"/>
</span>

Módulo de gerência de Contas a Pagar.

![Contas a Pagar Index](./img/contas-pagar__index.png)

## Opções de Menu

* Arquivo
    * Histórico Contas a Pagar
    * Relatório Contas a Pagar Vencidas
* Resumo
    * Parcelas Pagas
* Gráfico
    * Parcelas Pagas
