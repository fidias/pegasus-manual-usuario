---
prev: /web/
---

# Livro de Caixa

<span class="img-96">
    <img src="/img/modulo/livro-caixa.svg" alt="livro caixa"/>
</span>

Módulo de gerência de Livro de Caixa.

![Livro de Caixa Index](./img/livro-caixa__index.png)

## Opções de Menu

* Resumo
    * Livro de Caixa
    * Livro de Caixa - Contas a Pagar/Receber
* Gráficos
    * Livro de Caixa
    * Tipo Movimento
