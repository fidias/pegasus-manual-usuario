# Emitir NF-e (Nota Fiscal Eletrônica)

:::warning IMPORTANTE

Para emissão de NF-e é obrigatório:

- O certificado digital estar configurado no sistema e dentro da validade (caso
  o certificado ainda não esteja configurado entre em contato com o suporte).

- O destinatário deve estar cadastrado com **Nome/Razão Social**, **CPF/CNPJ** e
  **Endereço Completo**. No caso de CNPJ deve verificar se o mesmo possui 
  Inscrição Estadual. Ao informar a Inscrição Estadual deve também mudar o campo
  "**Indicador IE Destinatário**" para o código "**1 - Contribuinte ICMS**".

- Os produtos devem estar com seus dados fiscais preenchidos (**NCM**, **ICMS**,
  **PIS**, **COFINS**, **CFOP Saída Local/Interestadual**. Caso tenha dúvida
  sobre a informação a ser preenchida consulte a sua contabilidade.
:::

---

Há **2 formas** de emitir uma NF-e.

- [Importando a partir de uma venda](#importar-a-partir-de-uma-venda)
- [Preenchendo manualmente](#criar-nf-e-manualmente)

## Importar a partir de uma venda

:::tip
Essa é a forma mais recomendada para emissão de **NF-e de venda**. Dessa forma a
NF-e já vai com o destinatário, produtos, formas de pagamento, natureza de
operação já preenchidos. Além de dar baixa em estoque.
:::

1. No Frente de Caixa, crie um **DAV** do tipo **Pedido** ou **Ordem de
   Serviço**.

   ![lista dav](./img/importar_venda/lista_dav.png)

2. Informe o destinatário e os produtos que irão compor a NF-e. Depois, mude o
   status do **DAV** para **AG_FATURAMENTO**.

   ![novo dav](./img/importar_venda/novo_dav.png)

3. Volte para o caixa clicando no menu `Ir Para → Frente Caixa` (ou
   pressionando a tecla `F4`).

   ![ir para frente caixa](./img/importar_venda/dav_ir_para.png)

4. No caixa, pressione a tecla `F12` para listar os DAVs com o status
   **AG_FATURAMENTO**. Clique no botão `NF-e` da venda que se queira emitir uma
   **NF-e** e finalize a venda.

   ![importar dav NF-e](./img/importar_venda/importar_dav.png)

5. Vá para o **Pegasus Web** (pelo navegador) e abra o módulo **Vendas** (o
   ícone é uma caixa registradora).

   ![modulo vendas](./img/importar_venda/modulo_vendas.png)

6. Procure a venda e clique no botão **Importar NF-e** para transformá-la em
   NF-e. Se estiver tudo OK vai aparecer uma mensagem em um retângulo verde com
   uma mensagem de sucesso  em seguida irá mudar para a página da NF-e. Se não,
   vai aparecer um quadrado amarelo informando o que está faltando ou deve ser
   corrigido (dados de produtos, dados de emitente ou dados de destinatário).

   ![importar venda NF-e](./img/importar_venda/importar_nfe.png)

7. Como a NF-e foi gerada a partir de uma venda, pode-se deduzir que já está
   tudo OK (valor total da NF-e, impostos e CFOP dos itens, destinatário, formas
   de pagamento e etc). Então, desça até o final da página e clique no botão
   `Transmitir`.

   ![transmitir NF-e](./img/importar_venda/transmitir_nfe.png)

## Criar NF-e manualmente

::: tip
Essa forma é mais usada para NF-e com Natureza de Operação para **Devolução de
Mercadoria**, **Remessa em Garantia**, **Entrada**, **Bonificação** e outras.
Também serve para **Venda de Mercadoria Adquirida**. Porém, não faz baixa em
estoque.
:::

1. No **Pegasus Web**, clique no ícone **NF-e** e depois no botão verde **Novo**
   (é o botão de cima que **não** tem a palavra **DEV**).

   ![modulo NF-e](./img/manual/modulo_nfe.png)

2. Informe o cliente/fornecedor no campo **Destinatário**, **UF** e o
   **Município de  Ocorrência**. Aqui a UF e Município de Ocorrência é de onde
   está saindo a NF-e, ou seja, do emitente. Caso a NF-e esteja sendo emitida
   para um destinatário que não seja contribuinte de ICMS (que não possui
   Inscrição Estadual) deve indicar que o destinatário é um Consumidor Final.
   Depois, desça até o final da página e clique no botão **Salvar**.

   ![nova NF-e](./img/manual/nova_nfe.png)

3. Ao clicar no botão **Salvar** as outras abas são desbloqueadas. Clique na aba
   **Produtos/Serviços** e informe os itens e suas respectivas quantidades. Deve
   abrir uma caixa pra editar alguma coisa do item caso seja necessário. Se não
   precisar editar, então, é só clicar no botão **Salvar**.

   ![aba produto](./img/manual/aba_produto.png)

   ![salvar produto](./img/manual/salvar_produto.png)

4. Clique na aba **Info. Pagamento** e informe a forma de pagamento da compra.
   Para NF-e de **Devolução** não precisa preencher as informações de pagamento.
   Para NF-e de **Garantia** selecionar a forma de pagamento **Sem Pagamento**.

   ![aba pagamento](./img/manual/info_pagamento.png)

   ![salvar pagamento](./img/manual/salvar_pagamento.png)

5. Depois de tudo preenchido volte na aba **Dados NF-e**, vá até o final da
   página e clique no botão **Transmitir**.

   ![transmitir NF-e](./img/importar_venda/transmitir_nfe.png)

NOTAS:

- Uma NF-e de venda sempre vai ter a natureza da operação como **VENDA DE
   MERCADORIA ADQUIRIDA**.

- O botão de adicionar com a palavra **DEV** serve para pré preencher uma NF-e
  de Devolução. Informações como natureza de operação, CFOP, e Info. Adicionais.
