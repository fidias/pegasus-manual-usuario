---
prev: /web/
---

# Compras

<span class="img-96">
    <img src="/img/modulo/compra.svg" alt="compras"/>
</span>

Módulo de gerência de Compras.

![Compras Index](./img/compras__index.png)

## Opções de Menu

* Arquivo
    * Devolu&ccedil;&atilde;o de mercadoria
    * Visualizar Compra
    * Relat&oacute;rio Opera&ccedil;&atilde;o
    * Transportadora
    * Gerar Arquivo Fortes Fiscal AC
* Histórico
    * Compras
    * Opera&ccedil;&atilde;o
* Resumo
    * Da Opera&ccedil;&atilde;o
