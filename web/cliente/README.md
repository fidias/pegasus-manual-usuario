---
prev: /web/
---

# Cliente

<span class="img-96">
    <img src="/img/modulo/cliente.svg" alt="cliente"/>
</span>

Módulo de gerência de Clientes.

![Cliente Index](./img/cliente__index.png)

## Opções de Menu

* Arquivo
    * Histórico do Cliente
    * Contas a Receber/Recebidas
    * Hist&oacute;rico de Vendas Credi&aacute;rio - Clientes
    * Relat&oacute;rio Vendas no Balc&atilde;o - Cliente
    * Ranking Cliente - Vendas
    * Enviar E-Mail
    * Relatório Devoluções de Mercadoria e Vendas no Crédito Troca
    * Relatório Última Venda
    * Contratos
* Resumo
    * Contas a Receber - Todos os Clientes
* Recebimentos
    * Receber um valor
* Outros
    * Municípios
    * Bairros
