---
prev: /web/
---

# Caixa

<span class="img-96">
    <img src="/img/modulo/caixa.svg" alt="caixa"/>
</span>

Módulo de gerência de Caixa.

![Caixa Index](./img/caixa__index.png)

## Opções de Menu

* Arquivo
    * Relat&oacute;rio Movimento de Caixa
    * [Vendas](vendas.md)
