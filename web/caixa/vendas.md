---
prev: /web/caixa/
---

# Vendas

<span class="img-96">
    <img src="/img/modulo/vendas.svg" alt="vendas"/>
</span>

Módulo de gerência de Vendas.

![Vendas Index](./img/vendas__index.png)
