---
prev: /web/
---

# Serviço

<span class="img-96">
    <img src="/img/modulo/servico.svg" alt="servico"/>
</span>

Módulo de gerência de Serviço.

![Serviço Index](./img/servico__index.png)

## Opções de Menu

* Configuração
    * Código do Serviço
