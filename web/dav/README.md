---
prev: /web/
---

# DAV (Documento Auxiliar de Venda)

<span class="img-96">
    <img src="/img/modulo/dav.svg" alt="dav"/>
</span>

Módulo de gerência de DAV.

![DAV Index](./img/dav__index.png)

## Opções de Menu

* Veículo
    * Marcas
