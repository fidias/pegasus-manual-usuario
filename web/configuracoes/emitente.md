---
prev: /web/configuracoes/
---

# Dados do Emitente

Módulo de cadastro dos dados do Emitente.

![Emitente Index](./img/emitente__index.png)
