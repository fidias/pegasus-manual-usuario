---
prev: /web/configuracoes/
---

# Opções

Define configurações de comportamento do sistema, bem como valores padrão,
taxas, etc.

![Configuração - Opções](./img/configuracao__opcoes.png)

## Opções de Menu

* MF-e
    * [Auxiliar de Configuração](opcoes/mfe-aux-config.md)
