---
prev: /web/configuracoes/opcoes.md
---

# MF-e - Auxiliar de Configuração

<ul class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="/web/configuracoes/">Configurações do Sistema</a>
  </li>
  <li class="breadcrumb-item">
    <a href="/web/configuracoes/opcoes.html">Opções</a>
  </li>
  <li class="breadcrumb-item">
    <a href="#">MF-e - Auxiliar de Configuração</a>
  </li>
</ul>

Tela para auxiliar na geração da Chave de Requisição e Assinatura do AC.

![Auxiliar de Configuração](./img/configuracao__mfe-aux-config.png)

## Chave de Requisição

A Chave de Requisição é formada pelo CNPJ da Filial, ou seja, o Emitente configurado do sistema,
e o CNPJ do Adquirente, ou Operadora de Cartão. A Chave é gerada a partir da concatenação
desses dois CNPJ's resultará em um UUID.

Exemplo de Chave de Requisição:

```
26359854-5698-1365-9856-965478231456
```

O procedimento de geração da Chave de Requisição deve ser realizado no Cliente,
usando o CNPJ configurado em [Dados do Emitente](../emitente.md). O adquirente deve ser escolhido
com base nos documentos do POS que o cliente adquiriu. Para cada POS utilizado
na loja, será gerado uma Chave de Requisição diferente.

## Assinatura AC

A Assinatura AC é formada pelo CNPJ da _Software House_, e o CNPJ do Estabelecimento
do Cliente. Ambos CNPJ's são concatenados e assinados com o Certificado Digital
da _Software House_ e convertido em Base64.

Exemplo de Assinatura AC:

```
MD2Nof/aAs0rulXz948bZla0eXABgEcp6mDkTzweLPZTbmOhX+eA==
```

A geração da Assinatura AC deve ser feita em <https://pegasus.fidias.com.br/pegasus>,
usando o Certificado Digital da Fidias. Essa Assinatura Digital será usada pelo
Integrador para assinar o CF-e. Essa assinatura é única por cliente, ou seja,
se o cliente possuir mais de um caixa, todos eles utilizarão a mesma assinatura.
