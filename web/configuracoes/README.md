---
prev: /web/
---

# Menu Configurações do Sistema

<span class="img-96">
    <img src="/img/modulo/configuracao.svg" alt="configuracoes"/>
</span>

Menu de Configurações Gerais.

- Grupo de Usuários
- [Opções](opcoes.md)
- Usuário do Sistema
- Contas
- Grupo Mercadoria
- Limpar Movimento
- [Dados do Emitente](emitente.md)
- Cartões de Crédito
- Transportadora
- Dados do Contabilista
