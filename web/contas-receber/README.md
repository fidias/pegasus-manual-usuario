---
prev: /web/
---

# Contas a Receber

<span class="img-96">
    <img src="/img/modulo/contas-receber.svg" alt="contas receber"/>
</span>

Módulo de gerência de Contas a Receber.

![Contas a Receber Index](./img/contas-receber__index.png)

## Opções de Menu

* Arquivo
    * Receber parcelas
    * Gerar carnês
    * Preencher Recibo Avulso
    * Gerar Recibo Avulso
    * Relatório Contas a Receber Vencidas
    * Relatório Vencimento Por Núm. Dias
    * Histórico Parcelas a Receber
* Resumo
    * Resumo Parcelas
* Gráficos
    * Parcelas Recebidas
    * Relatório Inadimplência
* Boleto
    * Gerar Remessa
