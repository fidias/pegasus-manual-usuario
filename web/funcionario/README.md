---
prev: /web/
---

# Funcionário

<span class="img-96">
    <img src="/img/modulo/funcionario.svg" alt="funcionario"/>
</span>

Módulo de gerência de Funcionários.

![Funcionário Index](./img/funcionario__index.png)

## Opções de Menu

* Arquivo
    * Comissão Histórico
* Resumo
    * Comissão Acumulada
    * Vendas do Entregador
