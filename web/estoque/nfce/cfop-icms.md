# NFC-e - CFOP permitido para ICMS

Ao enviar um NFC-e com CFOP não permitido para determinado ICMS, o seguinte erro
será retornado:

::: warning
386 - CFOP não permitido para o CSOSN informado
:::

## Uso

Quando for emitida uma NFC-e com o Código de Situação da Operação – Simples Nacional (CSOSN) igual à:

- 102 - Tributada pelo Simples Nacional sem permissão de crédito;
- 103 - Isenção do ICMS no Simples Nacional para faixa de receita bruta;
- 300 - Imune;
- 400 - Não tributada pelo Simples Nacional;
- 900 - Outros (a critério da UF).

Poderão ser usados os seguintes CFOP's:

- 5101 - Venda de produção do estabelecimento;
- 5102 - Venda de mercadoria de terceiros;
- 5103 - Venda de produção do estabelecimento, efetuada fora do estabelecimento;
- 5104 - Venda de mercadoria adquirida ou recebida de terceiros, efetuada fora do estabelecimento;
- 5115 - Venda de mercadoria de terceiros, recebida anteriormente em consignação mercantil;

---

Quando for emitida uma NFC-e com o Código de Situação da Operação – Simples Nacional (CSOSN) igual à:

- 500 - ICMS cobrado anteriormente por substituição tributária (substituído) ou por antecipação.

Poderão ser usados os seguintes CFOP's:

- 5405 - Venda de mercadoria de terceiros, sujeita a ST, como contribuinte substituído;
- 5656 - Venda de combustível ou lubrificante de terceiros, para consumidor final;
- 5667 - Venda de combustível ou lubrificante a consumidor ou usuário final estabelecido em outra unidade da Federação.

## Referências

- <https://www.oobj.com.br/bc/article/rejei%C3%A7%C3%A3o-386-cfop-n%C3%A3o-permitido-para-o-csosn-informado-como-resolver-217.html>
