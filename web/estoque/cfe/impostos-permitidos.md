# CF-e - Impostos permitidos

## CFOP

- 5101 - Venda de produção do estabelecimento
- 5102 - Venda de mercadoria adquirida ou recebida de terceiros
- 5103 - Venda de produção do estabelecimento, efetuada fora do estabelecimento
- 5405 - Venda de mercadoria adquirida ou recebida de terceiros em operação com mercadoria sujeita ao regime de substituição tributária, na condição de contribuinte substituído

## ICMS - Regime Normal

- 00 - Tributada Integralmente
- 20 - Tributação com redução de base de cálculo
- 90 - Outros
- 40 - Tributação Isenta
- 41 - Não tributada
- 60 - Tributação ICMS cobrado anteriormente por substituição tributária

## ICMS - Simples Nacional

- 102 - Tributada pelo Simples Nacional sem permissão de crédito
- 300 - Imune
- 400 - Não tributada pelo Simples Nacional
- 500 - ICMS cobrada anteriormente por ST ou por antecipação
- 900 - Tributação ICMS pelo Simples Nacional

## PIS

- 04 - Operação Tributável (tributação monofásica (alíquota zero))
- 06 - Operação Tributável (alíquota zero)
- 07 - Operação Isenta da Contribuição
- 08 - Operação Sem Incidência da Contribuição
- 09 - Operação com Suspensão da Contribuição
- 99 - Outras Operações

## COFINS

- 04 - Operação Tributável (tributação monofásica, alíquota zero)
- 06 - Operação Tributável (alíquota zero);
- 07 - Operação Isenta da Contribuição
- 08 - Operação Sem Incidência da Contribuição
- 09 - Operação com Suspensão da Contribuição
- 99 - Outras Operações

## Referências

- <https://www.oobj.com.br/bc/article/orienta%C3%A7%C3%B5es-de-preenchimento-de-campos-do-layout-do-cf-e-sat-481.html>
