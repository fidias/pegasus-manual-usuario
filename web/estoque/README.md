---
prev: /web/
---

# Estoque

<span class="img-96">
    <img src="/img/modulo/estoque.svg" alt="estoque"/>
</span>

Módulo de gerência de Estoque.

![Estoque Index](./img/estoque__index.png)

## Opções de Menu

* Arquivo
    * Relatório Venda no Balcão
    * Relatório Débito/Crédito - Cartão
    * Grupos de Mercadoria
    * Previsão de Compra
    * Relatório Estoque
    * Lista de Preços
    * Ranking 20+ vendidos
    * Custos
    * Relatório Quantidade Mínima
    * Curva ABC
    * Histórico de Compra e Venda
* Resumo
    * Ranking Vendas
    * Resumo das Vendas
    * Itens vendidos (sem mercadorias devolvidas)
    * Devoluções de Mercadoria
    * Resumo Venda de Produto por M&ecirc;s
* Gráficos
    * Vendas no Balcão
    * Vendas por dia da semana
* Outros
    * Gerar Etiquetas
    * Balancete (Inventário Físico)
    * Carregar Todas as Imagens
    * CFOP
    * Unidade

## NFC-e

- [CFOP permitido para ICMS](./nfce/cfop-icms.md)

## CF-e

- [Impostos permitidos](./cfe/impostos-permitidos.md)
