---
sidebar: false
---

# Pegasus Web - Módulos

<div class="modulos">
  <div class="modulo">
    <h3>Cliente</h3>
    <a href="/web/cliente/" class="img-96">
        <img src="/img/modulo/cliente.svg" alt="cliente"/>
    </a>
  </div>
  <div class="modulo">
    <h3>Fornecedor</h3>
    <a href="/web/fornecedor/" class="img-96">
        <img src="/img/modulo/fornecedor.svg" alt="fornecedor"/>
    </a>
  </div>
  <div class="modulo">
    <h3>Estoque</h3>
    <a href="/web/estoque/" class="img-96">
        <img src="/img/modulo/estoque.svg" alt="estoque"/>
    </a>
  </div>
  <div class="modulo">
    <h3>Livro de Caixa</h3>
    <a href="/web/livro-caixa/" class="img-96">
        <img src="/img/modulo/livro-caixa.svg" alt="livro caixa"/>
    </a>
  </div>
  <div class="modulo">
    <h3>Contas a Receber</h3>
    <a href="/web/contas-receber/" class="img-96">
        <img src="/img/modulo/contas-receber.svg" alt="contas receber"/>
    </a>
  </div>
  <div class="modulo">
    <h3>Contas a Pagar</h3>
    <a href="/web/contas-pagar/" class="img-96">
        <img src="/img/modulo/contas-pagar.svg" alt="contas pagar"/>
    </a>
  </div>
  <div class="modulo">
    <h3>Compras</h3>
    <a href="/web/compras/" class="img-96">
        <img src="/img/modulo/compra.svg" alt="compras"/>
    </a>
  </div>
  <div class="modulo">
    <h3>Menu Configurações</h3>
    <a href="/web/configuracoes/" class="img-96">
        <img src="/img/modulo/configuracao.svg" alt="configuracoes"/>
    </a>
  </div>
  <div class="modulo">
    <h3>Funcionário</h3>
    <a href="/web/funcionario/" class="img-96">
        <img src="/img/modulo/funcionario.svg" alt="funcionario"/>
    </a>
  </div>
  <div class="modulo">
    <h3>Caixa</h3>
    <a href="/web/caixa/" class="img-96">
        <img src="/img/modulo/caixa.svg" alt="caixa"/>
    </a>
  </div>
  <div class="modulo">
    <h3>Vendas</h3>
    <a href="/web/caixa/vendas.html" class="img-96">
        <img src="/img/modulo/vendas.svg" alt="vendas"/>
    </a>
  </div>
  <div class="modulo">
    <h3>DAV</h3>
    <a href="/web/dav/" class="img-96">
        <img src="/img/modulo/dav.svg" alt="dav"/>
    </a>
  </div>
  <div class="modulo">
    <h3>Serviço</h3>
    <a href="/web/servico/" class="img-96">
        <img src="/img/modulo/servico.svg" alt="servico"/>
    </a>
  </div>
  <div class="modulo">
    <h3>NF-e</h3>
    <a href="/web/nfe/" class="img-96">
        <img src="/img/modulo/nfe.svg" alt="nfe"/>
    </a>
  </div>
</div>
