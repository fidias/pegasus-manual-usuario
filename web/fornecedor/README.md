---
prev: /web/
---

# Fornecedor

<span class="img-96">
    <img src="/img/modulo/fornecedor.svg" alt="fornecedor"/>
</span>

Módulo de gerência de Fornecedor.

![Fornecedor Index](./img/fornecedor__index.png)

## Opções de Menu

* Arquivo
    * Histórico de Compras
    * Relatório Média Vendas
    * Ranking Vendas Por Fornecedor
