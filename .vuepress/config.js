module.exports = {
    title: 'Pegasus - Manual do Usuário',
    description: 'Descubra todas as possibilidades do nosso Sistema',
    themeConfig: {
        sidebar: 'auto',
        lastUpdated: 'Última edição',
        nav: [
            {
                text: 'Web',
                link: '/web/'
            },
            {
                text: 'Frente de Caixa',
                link: '/frente-caixa/'
            }
        ]
    },
    plugins: {
        '@vuepress/pwa': {
            serviceWorker: true,
            updatePopup: {
                message: "Novo conteúdo disponível.",
                buttonText: "Recarregar"
            }
        },
        '@vuepress/medium-zoom': {}
    }
}
