#!/usr/bin/env bash

cd /var/www/html/ || exit 1

BASE_DIR="pegasus-manual-usuario"
# backup previous version
if [[ -d $BASE_DIR ]]; then
    if [[ -d $BASE_DIR"-orig" ]]; then
        rm -r $BASE_DIR"-orig"
    fi

    mv $BASE_DIR{,-orig}
fi

unzip -q $BASE_DIR".zip" -d $BASE_DIR
