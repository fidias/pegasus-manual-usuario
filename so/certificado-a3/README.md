# Certificado A3

## Links para instalação dos drivers

- <https://www.certisign.com.br/duvidas-suporte/downloads/leitoras>
- <https://www.soluti.com.br/suporte/certificado-a3/>

## Para certificado A3 em cartão

É necessário primeiro instalar o driver para o leitor de certificado para que
possa ser reconhido pelo Windows (como um driver de adaptador de Wi-Fi) e depois
fazer o download do driver do certificado, que fará:

1. O registro das DLL's dos certificados A3 no Windows e;
1. Instalação de programa GUI para auxiliar na administração dos certificados
    instalados e conectados no computador (Safenet Client Tools).

## Para certificado A3 em token

O driver do token, após instalado, faz:

1. O registro das DLL's dos certificados A3 no Windows e;
1. Instala um programa GUI para auxiliar na administração dos certificados instalados
    e conectados no computador (Safenet Client Tools).
